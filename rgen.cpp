// Compile with c++ rgen
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <unistd.h>
using namespace std;
std::vector<std::vector<int>> generateRand(int s, int n, int l, int c);
bool checkValid(std::vector<std::vector<int>> street, std::vector<int> & coors, int x, int y);
void avoidSelfInter(std::vector<int> & coors);
bool isPointOnLine(vector<int> p, vector<int> l1, vector<int> l2);

int main(int argc, char** argv) {
	int s = 10;
	int n = 5;
	int l = 5;
	int c = 20;
	std::vector<std::string> cmd;
	
	//read in command line
	for (int i = 1; i < argc; i++) {
		cmd.emplace_back(argv[i]);
	}
	//store command argument value
	for (int j = 0; j < cmd.size(); j++) {
		if (cmd[j] == "-s") { s = stoi(cmd[j + 1]); }
		else if (cmd[j] == "-n") { n = stoi(cmd[j + 1]); }
		else if (cmd[j] == "-l") { l = stoi(cmd[j + 1]); }
		else if (cmd[j] == "-c") { c = stoi(cmd[j + 1]); }

	}
	if (s < 2 || n < 1 || l < 5 || c < 1){
		exit(0);
	}

	//random l value
	std::ifstream urandom("/dev/urandom");
	// read a random unsigned int
	unsigned int num = 0;
	urandom.read((char *)&num, sizeof(int));
	if (l > 5){
		l = (num % (l - 4)) + 5;
	}
	else{ l = 5; }
	int rmCount = 0;
	vector<string> nameVec;

	while (true){
		if (rmCount != 0){
			for (auto & rm: nameVec){
				string rmStr = "rm \"" + rm + "\"";
				cout << rmStr << endl;
				rmCount--;
			}
		}
		nameVec.clear();
		std::vector<std::vector<int>> street = generateRand(s,n,l,c);
		int i = 1;
		for (auto & a : street) {
			string name = "";	
			for (int j = 0; j < i + 1; j++){ name = name + "st"; }	
			nameVec.push_back(name);
			string res = "add \"" + name + "\" ";
			int count = 1;
			for (auto & b : a) {
				if (count % 2 == 0){
					res = res + to_string(b) + ")";
				}
				else { res = res + "(" + to_string(b) + ",";}
				count++;
			}
			cout << res << endl;
			rmCount++;
			i++;
		}
	cout << "gg" << endl;
	sleep(l);
	}

}



std::vector<std::vector<int>> generateRand(int s, int n, int l, int c) {
	// open /dev/urandom to read
	std::ifstream urandom("/dev/urandom");
	// read a random unsigned int
	unsigned int num = 0;
	urandom.read((char *)&num, sizeof(int));
	if (s > 2){
		s = (num % (s - 1)) + 2; //number of street
	}
	else {s = 2;}
	

	//vector store each street with size s
	std::vector<std::vector<int>> street;
	for (int i = 0; i < s; i++) {
		//random number of line segments
		urandom.read((char *)&num, sizeof(int));
		int line; //number of line segment
		if (n > 1){
			line = (num % n) + 1;
		}
		else {line = 1;}
		std::vector<int> coors; //store coordinates of line segments
		//generates coordinates
		for (int j = 0; j < line + 1; j++) {
			int x, y;
			// x coor
			bool valid = false;
			int A = 1;
			while(!valid){
				if (A == 25){
					cerr << "Error: failed to generate valid input for 25 attempts" << endl;
					exit(0);
				}
				urandom.read((char *)&x, sizeof(int));
				x = (x % (c + 1));
				//y coor
				urandom.read((char *)&y, sizeof(int));
				y = (y % (c + 1));
				coors.emplace_back(x);
				coors.emplace_back(y);
				valid = checkValid(street, coors, x, y);
				A++;
			}
		}
		street.emplace_back(coors);
	}

	// close random stream
	urandom.close();
	return street;
}

bool checkValid(std::vector<std::vector<int>> street, std::vector<int> & coors, int x, int y){

    	int size = coors.size();
	if (size == 2){return true;}
	//length is 0
	if (size >= 4 && x == coors[size - 4] && y == coors[size - 3]){
		coors.pop_back();
		coors.pop_back();
		return false;	
	}
	//check overlap
	vector<int> A = {coors[size - 4], coors[size - 3]};
	vector<int> B = {x, y};
	bool res = true;
	if (!street.empty()){
		for (auto & a : street){
			for (int i = 0; i < a.size()-2; i = i + 2){
				vector<int> C = {a.at(i), a.at(i+1)};
				vector<int> D = {a.at(i+2), a.at(i+3)};
				bool Aon, Bon;
				Aon = isPointOnLine(A, C, D);
				Bon = isPointOnLine(B, C, D);
				if (Aon && Bon){
					if (A[0] <= C[0] && A[1] <= C[1] && B[0] <= C[0] && B[1] <= C[1]){res = true;}
					else if (A[0] >= D[0] && A[1] >= D[1] && B[0] >= D[0] && B[1] >= D[1]){res = true;}
					else{return false; }
				}
			}
		}
		
	}
	//avoid self intersection
	avoidSelfInter(coors);
	return true;
}

void avoidSelfInter(std::vector<int> & coors){
	vector<pair<int,int>> vec;
	for (int i = 0; i < coors.size();i = i + 2){
		vec.push_back(make_pair(coors[i],coors[i+1]));
	}
	sort(vec.begin(), vec.end());
	coors.clear();
	for (auto & a : vec){
		coors.push_back(a.first);
		coors.push_back(a.second);	
	}
}

bool isPointOnLine(vector<int> p, vector<int> l1, vector<int> l2){
	float position = (l2[1] - p[1]) * (l1[0] - p[0]) - (l1[1] - p[1]) * (l2[0] - p[0]);
	if (fabs(position) < 1e-6){
		return true;
	}
	else{
		return false;	
	}
}











