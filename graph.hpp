#pragma once

#include <string>
#include <vector>
#include <algorithm>
// defined std::unique_ptr
#include <memory>
// defines Var and Lit
#include "minisat/core/SolverTypes.h"
// defines Solver
#include "minisat/core/Solver.h"

/// Graph class, store the vertex and edge and generate graph
class Graph
{
   
    /// vertex
    int ver;
	///edges
	std::vector<int> edge;
	///represent graph in matrix
	std::vector<std::vector<int>> matrix;
	///color used in BFS 'w' for white, 'b' for black, 'g' for grey
	std::vector<char> color;
	///distance used in BFS
	std::vector<int> d;
	///store predecessor of vertex
	std::vector<int> pi;
 public:
    /// Constructor. Creates a graph with empty ver and edge
    Graph();
	/// set the value into ver vector
	void setVer(int i);
	int getVer();
	std::vector<int> getEdge();
	///set the value into edge vector
	void setEdge(std::vector<int> e);
        /// Clear the ver and edge vector
    	void reset();
	///represent graph with matrix
	void generateGraph();
	///get the shortest path
	bool getPath(int s, int e, std::string &err_msg,  std::vector<int> & shortest);
};

void miniVC(Graph g);
void atLeastOne(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x);
void noTwice(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x);
void noMoreMPosition(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x);
void everyIncident(std::vector<int> edge, std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x);
std::vector<int> checkSAT(std::vector<std::vector<int>> clause, int k, int n);
int getIndex(int a, std::vector<std::vector<int>> x);
/**
 * Parses a command line.
 * Returns a character of a command and argument.
 * Returns true on success and false on a parsing error.
 * On error, err_msg contains the error message
 */
bool parse_line (const std::string &line,
                 char &cmd, int &arg, std::vector<int> &args,std::string &err_msg);
