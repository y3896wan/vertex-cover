#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
using namespace std;
int main (int argc, char **argv) {
	
	vector<pid_t> kids;
	int RtoP[2]; //pipe between rgen to python code
	int PtoC[2]; // pipe between python code to c++ pj2 code
	pipe(RtoP);
	pipe(PtoC);
	
	pid_t child_pid;
	child_pid = fork();
	if (child_pid == 0){
		dup2(RtoP[1], STDOUT_FILENO);
		close(RtoP[0]);
		close(RtoP[1]);
		close(PtoC[0]);
		close(PtoC[1]);
		//exec program rgen
		execv("rgen",argv);
		return 0;

	}else if (child_pid < 0){
		cerr << "Error: could not fork\n";
		return 1;
	}
	kids.push_back(child_pid);
	
	child_pid = fork();
	if (child_pid == 0) {
		dup2(PtoC[0], STDIN_FILENO);
		close(RtoP[0]);
		close(RtoP[1]);
		close(PtoC[0]);
		close(PtoC[1]);
		//exec program
		execv("ece650-a2", nullptr);
		return 0;
	}else if (child_pid < 0){
		cerr << "Error: could not fork\n";
		return 1;
	}
	kids.push_back(child_pid);

	child_pid = fork();
	if (child_pid == 0){
		dup2(PtoC[1],STDOUT_FILENO);
		close(RtoP[0]);
		close(RtoP[1]);
		close(PtoC[0]);
		close(PtoC[1]);
		while (!cin.eof()){
			string line;
			bool lineEmpty = true;
			getline(cin, line);
			for (char a : line){
				if (!isspace(a)){
					lineEmpty = false;
					break;
				}
			}
			if (line.size() == 0 || lineEmpty == true){
				int status;
				for (pid_t i : kids){
					kill(i,SIGTERM);
					waitpid(i, &status,0);
				}
				exit(0);
			}
			cout << line << endl;
		}
		return 0;
	}else if (child_pid < 0){
		cerr << "Error: could not fork\n";
		return 1;
	}

	kids.push_back(child_pid);

	child_pid = fork();
	if (child_pid == 0){
		dup2(RtoP[0], STDIN_FILENO);
		dup2(PtoC[1],STDOUT_FILENO);
		close(RtoP[0]);
		close(RtoP[1]);
		close(PtoC[0]);
		close(PtoC[1]);
		//exec python program
		execl("/usr/bin/python3","python3","ece650-a1.py", nullptr);
		return 0;
	}else if (child_pid < 0){
		cerr << "Error: could not fork\n";
		return 1;
	}
	kids.push_back(child_pid);

	for (pid_t k :kids){
		int status;
		waitpid(k, &status, 0);
		if (WIFEXITED(status)){
			for (pid_t i : kids){
				kill(i, SIGTERM);
				waitpid(i, &status, 0);
			}
		}
	}	
	return 0;
}

















