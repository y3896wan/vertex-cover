// Compile with c++ ece650-a2cpp -std=c++11 -o ece650-a2
#include <iostream>
#include <sstream>
#include <vector>
#include "graph.hpp"

int main(int argc, char** argv) {
	Graph g;
	while (!std::cin.eof()) {
		// read a line of input until EOL and store in a string
		std::string line;
		std::getline(std::cin, line);
		// if nothing was read, go to top of the while to check for eof
		if (line.size() == 0) {
			continue;
		}
		char cmd;
		int arg; // store one argument such as vertex number
		std::vector<int> args; //store arguments more than one value
		std::string err_msg; 

		//parse line and do action based on cmd
		if (parse_line(line, cmd, arg, args, err_msg)) {
			switch (cmd) {
			case 'E':{
				g.reset(); //reset graph
				g.setVer(arg); //set vertex
				g.setEdge(args); //set edge
				g.generateGraph(); //generate a new graph based on vertex and edge
			        args.clear();
				std::string vstr = "V " + std::to_string(g.getVer());
				std::cout << vstr << std::endl;
				std::string estr = "E {";
				int i = 1;
				for (auto & es : g.getEdge()){
					if (i % 2 == 0 ){
						estr = estr + std::to_string(es) + ">,";
					}
					else{
						estr = estr + "<" + std::to_string(es) + ",";
					}
					i++;
				}
				estr.back() = '}';
				if (g.getVer() == 0){
					estr = "E {}";
				}
				std::cout << estr << std::endl;
				break;}
			case 's':{
				int start = args[0];
				int end = args[1];

				g.BFS(start); //doing BFS of graph
				std::vector<int> shortest;
				bool b = g.getPath(start, end, err_msg, shortest); //find the shortest path
				if (!b) {
					shortest.clear();
					std::cerr << err_msg << "\n"; //no path, return error
				}
				else {
					//output shortest path
					for (int i = 0; i < shortest.size(); i++) {
						std::cout << shortest.at(i);
						if (i != shortest.size() - 1) {
							std::cout << '-';
						}
					}
					std::cout << "\n";
				}
				args.clear();
				break;}
			}
		}
		else {
			//output error message
			args.clear();
			if (cmd != 's') {
				//if command is V and E, reset the graph, if command is s, clear nothing
				g.reset();   
			}
			std::cerr << err_msg << "\n";
		}
		
	}
   
}
