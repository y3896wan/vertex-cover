#!/usr/bin/env python

import sys

class Street(object):
    def __init__(self, name, position):
        self.name = name
        self.position = position
    
    def getName(self):
        return self.name
    def getPosition(self):
        return self.position
    def setName(self, n):
        self.name = n
    def setPosition(self, p):
        self.position.append(p)
    def positionClr(self):
        self.position.clear()
		
class Line(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
		
def main():
    # YOUR MAIN CODE GOES HERE

    # sample code to read from stdin.
    # make sure to remove all spurious print statements as required
    # by the assignment
    streetSet = []
    verDict = {}
    vid = 1
    while True:
        
        line = sys.stdin.readline()
        if line == "":
            break
        try:
            cmd, street = parseLine(line)
            if cmd == 'add':
                addStreet(streetSet, street)
            elif cmd == 'mod':
                modStreet(streetSet, street)
            elif cmd == 'rm':
                rmStreet(streetSet, street)
            elif cmd == 'gg':
                lineList = []
                verSet = set()
                vertex = makeV(streetSet, lineList, verSet)
                vid = setIntoDict(vertex, verDict, vid)
                arrangeID(verDict)
                edgeDic = makeE(verSet, verDict, vertex)
                printStdOut(verDict, edgeDic)
   
        except Exception as e:
            print(str(e), file = sys.stderr)


    # return exit code 0 on successful termination
    sys.exit(0)

def arrangeID(verDict):
    i = 1
    for key, item in verDict.items():
        key = str(i)
        i = i + 1

def printStdOut(verDict, edgeDic):
    print("V", len(verDict),file = sys.stdout)
    if len(edgeDic) == 0:
        print("E {}", file = sys.stdout)
    else:
        print("E {",end = "", file = sys.stdout)  
        for i in edgeDic[:-1]:
            print("<"+i[0]+","+i[1]+">"+",", end = "",file = sys.stdout)
        if len(edgeDic) != 0:
            print("<"+edgeDic[-1][0]+","+edgeDic[-1][1]+">}", file = sys.stdout)
    sys.stdout.flush()



def makeE(verSet, verDict, vertex):
    verlist = list(verSet)
    edge = []
    edgeDict = []
    for i in verlist:
        valid = checkEdgeValid(i[0],i[1], vertex)
        if not valid:
            edge.append([i[0],i[1]])
        valid2 = checkEdgeValid(i[1],i[2], vertex)
        if not valid2:
            edge.append([i[1],i[2]])
			
    for j in range (0, len(verlist)):
        for k in range (j+1, len(verlist)):
            if (verlist[j][0] == verlist[k][0]) and (verlist[j][2] == verlist[k][2]):
               valid3 = checkEdgeValid(verlist[j][1],verlist[k][1], vertex)
               if not valid3:
                   edge.append([verlist[j][1],verlist[k][1]])
    for x in edge:
        if x[0] != x[1]:
            keyPair = (getKey(x[0], verDict), getKey(x[1],verDict))
            edgeDict.append(keyPair)
        #if valid, search verDict and give keys  
    return edgeDict
	

def getKey(item, verDict):
    for key, entry in verDict.items():
        if item == entry:
            return key
def checkEdgeValid(p1,p2,vertex):
    onLine = False
    for i in vertex:
        if (p1 == i) or (p2 == i):
            continue
        a1 = "{0:.2f}".format((i[0]-p1[0])*(p2[1]-p1[1]))
        a2 = "{0:.2f}".format((p2[0]-p1[0])*(i[1]-p1[1]))
        if a1 == a2:
            if (min(p1[0],p2[0]) <= i[0]) and (i[0] <= max(p1[0],p2[0])) and (min(p1[1],p2[1]) <= i[1]) and (i[1] <= max(p1[1],p2[1])):
                onLine = True
                break
            else: onLine = False
        else : onLine = False
    return onLine
            
    
def setIntoDict(vertex, verDict, vid):
    num = 1;
    for i in vertex:
        if i not in verDict.values():
            verDict[str(num)] = i
            num = num + 1
    inVertex = False
    keyList = []
    for key in verDict:
        for j in vertex:
            if verDict[key] == j:
                inVertex = True
        if inVertex == False:
            keyList.append(key)
        inVertex = False
    for x in keyList:
        verDict.pop(x)
    return vid
	
	
def makeV(streetSet,lineList, verSet):
    for i in streetSet:
        p = i.getPosition()
        line = []
        for j in range (0,len(p)-1): 
            l = Line(p[j], p[j+1])
            line.append(l)
        lineList.append(line)
    interSet = set()
    for x in lineList:
        for y in lineList[1:]:
            for i in x:
                for j in y:
                    findIntersection(i, j, interSet, verSet)				
    #for x in range (0,len(lineList)-1):
    #    for y in lineList[x]:
    #        for z in lineList[x+1]:
    #            findIntersection(y, z, interSet)
     
        
    return interSet
	
def findIntersection(l1,l2,interSet, verSet):
    #the below algorithm to find the intersection 
	#referenced by py_intersect.py in gitLab
    x1, y1 = float(l1.start[0]), float(l1.start[1])
    x2, y2 = float(l1.end[0]), float(l1.end[1])
    x3, y3 = float(l2.start[0]), float(l2.start[1])
    x4, y4 = float(l2.end[0]), float(l2.end[1])
    onLine = False
    xnum = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4))
    xden = ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4))
    if xden != 0 :
        xcoor =  xnum / xden
        onLine = True
    else:
        onLine = False
    
    ynum = (x1*y2 - y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)
    yden = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)
    if yden != 0 and onLine == True:
        ycoor = ynum / yden
    else:
        onLine = False
    #check line 1
    if onLine:
        if (x1 == x2):
            if (ycoor > min(y1,y2)) and (ycoor < max(y1,y2)):
                onLine = True
            else:
                onLine = False
        elif (y1 == y2):
            if (xcoor > min(x1,x2)) and (xcoor < max(x1,x2)):
                onLine = True
            else:
                onLine = False
        else:
            if xcoor == x1 and ycoor == y1:
                onLine = True
            elif xcoor == x2 and ycoor == y2:
                onLine = True
            elif (ycoor > min(y1,y2)) and (ycoor < max(y1,y2)) and (xcoor > min(x1,x2)) and (xcoor < max(x1,x2)):
                onLine = True
            else:
                onLine = False
    if onLine:
        if (x3 == x4):
            if (ycoor > min(y3,y4)) and (ycoor < max(y3,y4)):
                onLine = True
            else:
                onLine = False
        elif (y3 == y4):
            if (xcoor > min(x3,x4)) and (xcoor < max(x3,x4)):
                onLine = True
            else:
                onLine = False
        else:
            if xcoor == x3 and ycoor == y3:
                onLine = True
            elif xcoor == x4 and ycoor == y4:
                onLine = True
            elif (ycoor > min(y3,y4)) and (ycoor < max(y3,y4)) and (xcoor > min(x3,x4)) and (xcoor < max(x3,x4)):
                onLine = True
            else:
                onLine = False
    if onLine:
        interSet.add((x1,y1))
        interSet.add((x2,y2))
        interSet.add((x3,y3))
        interSet.add((x4,y4))
        interSet.add((xcoor,ycoor))
        verSet.add(((x1,y1),(xcoor,ycoor),(x2,y2)))
        verSet.add(((x3,y3),(xcoor,ycoor),(x4,y4)))

def rmStreet(streetSet, street):
    match = False
    if len(streetSet) == 0:
        match = False
    else:
        for st in streetSet:
            if st.getName() == street.getName():
                match = True
                streetSet.remove(st)
    if match == False:
        raise Exception('Error: no such street can be removed')

 
def modStreet(streetSet, street):
    match = False
    for st in streetSet:
        if st.getName() == street.getName():
            match = True
            st.positionClr()
            for pos in street.getPosition():
                st.setPosition(pos)
    if match == False:
        raise Exception('Error: no such street can be modified')

def addStreet(streetSet, street):

    addIn = False
    if len(streetSet)==0:
        addIn = True
    else:
        addIn = True
        for st in streetSet:
            if st.getName() == street.getName():
                addIn = False
                raise Exception('Error: the street already have')
    if addIn:
        streetSet.append(street)

def parseLine(line):
    street = Street("", [])
    sp = line.strip().split()
    cmd = sp[0]
    if (len(sp)) < 1:
        raise Exception('Error: too few arguments')
    
    if (cmd == "add" or cmd == "mod"):
        sp = line.strip().split('"')
        if (len(sp)) < 3:
            raise Exception('Error: street name should in the quotes')
        if sp[2][0] != ' ':
            raise Exception('Error: invalid format about whitespace')
        for j in sp[1]:
            if j.isalpha() == False and j.isspace() == False:
                raise Exception('Error: invalid street name')
        street.setName(sp[1].lower())
		
        addline = sp[2].strip().split("(")
        if len(addline) < 3:
            raise Exception('Error: the coordinators are invalid')
        addline.remove('')
        
        for i in addline:
            i = i.strip(" \n")
             
            if i[-1] != ")":
                raise Exception('Error: invalid argument, coordinate missed bracket')
            xy_str = i[:-1].split(',')
            if len(xy_str) != 2:
                raise Exception('Error: invalid argument, invalid coordinates')
            x_is_neg = xy_str[0].startswith('-') and xy_str[0][1:].isdigit()
            y_is_neg = xy_str[1].startswith('-') and xy_str[1][1:].isdigit()
            if xy_str[0].isdigit() == False and x_is_neg == False:
                raise Exception('Error: invalid argument, coordinates should be 2 integers')
            if xy_str[1].isdigit() == False and y_is_neg == False:
                raise Exception('Error: invalid argument, coordinates should be 2 integers')
            street.setPosition([int(xy_str[0]), int(xy_str[1])])
        
    elif (cmd == "gg"):
        if (len(sp)) > 1:
            raise Exception('Error: invalid argument of "gg"')
    elif (cmd == "rm"):
        rmline = line.strip().split('"')
        if (len(rmline)) != 3:
            raise Exception('Error: invalid argument of "rm" ')
        
        for j in rmline[1]:
            if j.isalpha() == False and j.isspace() == False:
                raise Exception('Error: invalid street name')
        street.setName(rmline[1].lower())
        street.setPosition([])
    else:
        raise Exception('Error: invalid command')

    return cmd, street
















if __name__ == "__main__":
    main()
