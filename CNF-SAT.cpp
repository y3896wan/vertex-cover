#include <iostream>
#include <sstream>
#include <vector>
#include "graph.hpp"

int main(int argc, char** argv) {
	Graph g;
	while (!std::cin.eof()) {
		// read a line of input until EOL and store in a string
		std::string line;
		std::getline(std::cin, line);
		// if nothing was read, go to top of the while to check for eof
		if (line.size() == 0) {
			continue;
		}
		char cmd;
		int arg; // store one argument such as vertex number
		std::vector<int> args; //store arguments more than one value
		std::string err_msg; 

		//parse line and do action based on cmd
		if (parse_line(line, cmd, arg, args, err_msg)) {
			switch (cmd) {
			case 'E':
				g.reset(); //reset graph
				g.setVer(arg); //set vertex
				g.setEdge(args); //set edge
				g.generateGraph(); //generate a new graph based on vertex and edge
			        args.clear();
				miniVC(g);
				break;
			
			}
		}
		else {
			//output error message
			args.clear();
			g.reset();   
			
			std::cerr << err_msg << "\n";
		}
		
	}
   
}
