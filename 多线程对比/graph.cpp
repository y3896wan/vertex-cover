// Calculator
#include "graph.hpp"

#include <iostream>
#include <sstream>
#include <string>
using namespace std;

Graph::Graph() {}

void Graph::setVer(int i) {
	ver = i;
}

int Graph::getVer(){
	return ver;
}
void Graph::setEdge(std::vector<int> e) {
	for (int i = 0; i < e.size(); i++) {
		edge.push_back(e.at(i));
	}
}

std::vector<int> Graph::getEdge(){
	return edge;
}

void Graph::generateGraph() {
	std::vector<std::vector<int>> temp(ver, std::vector<int>(ver));
	for (int i = 0; i < edge.size(); i = i + 2) {
		temp[edge.at(i) - 1][edge.at(i + 1) - 1] = 1;
		temp[edge.at(i + 1) - 1][edge.at(i) - 1] = 1;
	}
	matrix = temp;
}

void Graph::reset() { 
	ver = 0;
	edge.clear();
	matrix.clear();
	color.clear();
	d.clear();
	pi.clear();
}



bool Graph::getPath(int s, int e, std::string &err_msg, std::vector<int> & shortest) {
	//push the vertex that in the shortest path into a vector recursivly
	//the shortest vector finnaly store the shortest path 
	if (s == e) {
		shortest.push_back(s);
		return true;
	}
	else if (pi[e - 1] == 0) {
		err_msg = "Error: no path exists";
		return false;
	}
	else {
		bool b = getPath(s, pi[e - 1], err_msg, shortest);
		shortest.push_back(e);
		return b;
	}

}

vector<int> miniVC(Graph g){
	vector<int> res;
	int n = g.Graph::getVer();
	if (g.getEdge().size() == 0){
		return res;
	}
	for (int k = 0; k < n + 1; k++){
		int atomic = 1;
 	       //generate variable
		std::vector<std::vector<int>> x(n+1, std::vector<int>(k+1));
		for(int i = 1; i < n+1; i++){
			for (int j = 1; j < k+1; j++){
				x[i][j] = atomic;
				atomic++;
			}
		}
    
	        //generate clause
		vector<vector<int>> clause;
		//at least one vertex is the ith vertex in the cover
	        atLeastOne(clause, k, n, x);
		// no one vertex can appear twice in a vertex cover
	        noTwice(clause, k, n, x);
		//no more than one appears in the mth position of the vertex cover
	        noMoreMPosition(clause, k, n, x);
		//every edge is incident to at least one vertex in the vertex cover
	        everyIncident(g.getEdge(), clause, k, n,x);
	
	
		vector<int> mini = checkSAT(clause, k, n);
		sort(mini.begin(), mini.end());
		if (!mini.empty()){
			for (auto a : mini){
				int v = getIndex(a, x);
				res.push_back(v);
			}
			return res;
		}
	}
	
}


vector<int> Approch_1(Graph g){
	vector<int> res;
	vector<int> edges = g.getEdge();
	
	while (!edges.empty()){
		vector<int> deg(g.getVer()+1, 0);
		for (auto e : edges){
			if (e != 0){
				deg[e] = deg[e] + 1;
			}
   		}
		vector<int> sortDeg = deg;
		sort(sortDeg.rbegin(), sortDeg.rend());
		vector<int>::iterator itr = std::find(deg.begin(), deg.end(), sortDeg[0]);
		int highest = std::distance(deg.begin(), itr);
		res.push_back(highest);
		for (int i = 0; i < edges.size(); i = i + 2){
			if (edges[i] == highest || edges[i+1] == highest){
				edges[i] = 0;
				edges[i+1] = 0;
			}
		}
		bool zeros = std::all_of(edges.begin(), edges.end(),[](int i) {return i==0;});
		if (zeros == true){edges.clear();}
		
	}
	sort(res.begin(), res.end());
	return res;

}

vector<int> Approch_2(Graph g){
	vector<int> res;
	vector<int> temp;
	vector<int> edges = g.getEdge();
	temp.push_back(edges[0]);
	temp.push_back(edges[1]);
	while (!edges.empty()){
		
		int u = temp[0];
		int v = temp[1];
		temp.clear();
		res.push_back(u);
		res.push_back(v);
		for (int i = 0; i < edges.size(); i = i + 2){
			if (edges[i] == u || edges[i] == v || edges[i+1] == u || edges[i+1] == v){
				edges[i] = 0;
				edges[i+1] = 0;
			}
			else{
				temp.push_back(edges[i]);
				temp.push_back(edges[i+1]);
			}
		}
		bool zeros = std::all_of(edges.begin(), edges.end(),[](int i) {return i==0;});
		if (zeros == true){edges.clear();}

	}
	sort(res.begin(),res.end());
	return res;
}

int getIndex(int a, vector<vector<int>> x){

	int i = 0;
	int res = 0;
	for (auto r : x){
		for (auto l : r){
			if (l == a){
				res = i;
			}
		}
		i++;
	}
	return res;

}

vector<int> checkSAT(std::vector<std::vector<int>> clause, int k, int n){
	// -- allocate on the heap so that we can reset later if needed
	std::unique_ptr<Minisat::Solver> solver(new Minisat::Solver());
	vector<Minisat::Lit> l;
	vector<int> mini;
	for (int i = 0; i < n * k; i++){
		l.push_back(Minisat::mkLit(solver->newVar()));
	}

	for (auto c : clause){
		Minisat::vec<Minisat::Lit> lits;
		for (auto a : c){
			if (a > 0){
				lits.push(l[a - 1]);
			}else{
				int neg = -a;
				lits.push(~l[neg - 1]);
			}
			
		}
		solver->addClause(lits);
	}
	bool res = solver->solve();
	if (res){
		int index = 1;
		for (auto as : l){
			if (Minisat::toInt(solver->modelValue(as)) == 0){
				mini.push_back(index);
			}
			index++;
		}
	}
	return mini;
}

void atLeastOne(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x){

	for (int i = 1; i < k + 1; i++){
		vector<int> c;
		for (int j = 1; j < n + 1; j++){
			c.push_back(x[j][i]);
		}
		clause.push_back(c);
	}	
}

void noTwice(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x){

	for (int i = 1; i < n + 1; i++){
		
		for (int p = 1; p < k + 1; p++){
			for (int q = 1; q < k + 1; q++){
				vector<int> c;
				if (p < q){
					c.push_back(-x[i][p]);
					c.push_back(-x[i][q]);
					clause.push_back(c);
				}
			}
		}
		
	}
}

void noMoreMPosition(std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x){

	for (int i = 1; i < k + 1; i++){
		
		for (int p = 1; p < n + 1; p++){
			for (int q = 1; q < n + 1; q++){
				vector<int> c;
				if (p < q){
					c.push_back(-x[p][i]);
					c.push_back(-x[q][i]);
					clause.push_back(c);
				}
			}
		}
		
	}
}

void everyIncident(std::vector<int> edge, std::vector<std::vector<int>> &clause, int k, int n, std::vector<std::vector<int>> x){
	for(int e = 0; e < edge.size(); e = e + 2){
		int i = edge.at(e);
		int j = edge.at(e + 1);
		vector<int> c;
		for (int a = 1; a < k + 1; a++){
			c.push_back(x[i][a]);
			c.push_back(x[j][a]);
		}
		clause.push_back(c);
	}
}










bool parse_line (const std::string &line,
                 char &cmd, int &arg, std::vector<int> &args,std::string &err_msg) {

    std::istringstream input(line);
    // remove whitespace
    std::ws(input);

    char ch;
    input >> ch;

	if (ch == 'V') {
		int i;
		input >> i;
		std::ws(input);
		if (!input.eof()) {
			err_msg = "Error: Unexpected argument";
			return false;
		}
		cmd = ch;
		arg = i;
		return true;
	}
	else if (ch == 'E') {
		char symbol;
		input >> symbol; 
		if (input.peek() == '}'){
			cmd = ch;
			args.clear();
			return true;
		}

		if (arg <= 1) {
			err_msg = "Error: V must greater than 1";
			return false;
		}
		
		while (!input.eof() && symbol != '}') {
			input >> symbol;
			int num;
		    input >> num;
			if (num < 1 || num > arg) {
				err_msg = "Error: vertex does not exist";
				return false;
			}
			args.push_back(num);
			input >> symbol;
			input >> num;
			if (num < 1 || num > arg) {
				err_msg = "Error: vertex does not exist";
				return false;
			}
			args.push_back(num);
			input >> symbol;
			input >> symbol;
		}
		cmd = ch;
		return true;
	}
	else {
		err_msg = "Error: Unknown command";
		return false;
	}
    
}
