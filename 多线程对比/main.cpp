#include <iostream>
#include <sstream>
#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include "graph.hpp"

pthread_mutex_t lock;



void *CNF(void * data){
	std::stringstream out;
	out << "CNF-SAT-VC: ";
	pthread_mutex_lock(&lock);
	Graph *g = static_cast<Graph *>(data);

	
	std::vector<int> res = miniVC(*g);

	if (res.size() != 0 ){
		for (int i = 0; i < res.size(); i++){
			if (i != res.size() - 1){
				out << res[i] << ",";
			}
			else{ out << res[i];}
		}
	}
	std::cout << out.str() << std::endl;

	pthread_mutex_unlock(&lock);
	return 0;

}

void *VC1(void * data) {

	std::stringstream out;
	out << "APPROX-VC-1: ";
	pthread_mutex_lock(&lock);
	Graph *v1g = static_cast<Graph *>(data);

	
	std::vector<int> res = Approch_1(*v1g);

	if (res.size() != 0 ){
		for (int i = 0; i < res.size(); i++){
			if (i != res.size() - 1){
				out << res[i] << ",";
			}
			else{ out << res[i];}
		}
	}
	std::cout << out.str() << std::endl;

	pthread_mutex_unlock(&lock);
	return 0;
}

void *VC2(void * data) {

	std::stringstream out;
	out << "APPROX-VC-2: ";
	pthread_mutex_lock(&lock);
	Graph *v2g = static_cast<Graph *>(data);

	
	std::vector<int> res = Approch_2(*v2g);

	if (res.size() != 0 ){
		for (int i = 0; i < res.size(); i++){
			if (i != res.size() - 1){
				if (res[i] != 0){
					out << res[i] << ",";
				}
			}
			else{ out << res[i];}
		}
	}
	std::cout << out.str() << std::endl;

	pthread_mutex_unlock(&lock);
	return 0;
}

int main(int argc, char** argv) {

	pthread_mutex_init(&lock, nullptr);
	
	pthread_t t1, t2, t3;

	Graph g;
	while (!std::cin.eof()) {
		
		// read a line of input until EOL and store in a string
		std::string line;
		std::getline(std::cin, line);
		// if nothing was read, go to top of the while to check for eof
		if (line.size() == 0) {
			continue;
		}
		char cmd;
		int arg; // store one argument such as vertex number
		std::vector<int> args; //store arguments more than one value
		std::string err_msg; 

		//parse line and do action based on cmd
		if (parse_line(line, cmd, arg, args, err_msg)) {
			switch (cmd) {
			case 'E':
				g.reset(); //reset graph
				g.setVer(arg); //set vertex
				g.setEdge(args); //set edge
				g.generateGraph(); //generate a new graph based on vertex and edge
			        args.clear();
				
				pthread_create(&t1, nullptr, &VC2, &g);
				
				pthread_create(&t2, nullptr, &VC1, &g);
				
				pthread_create(&t3, nullptr, &CNF, &g);
				
				
				
				pthread_join(t1, nullptr);
				pthread_join(t2, nullptr);
				
				pthread_join(t3, nullptr);
				break;
			
			}
		}
		else {
			//output error message
			args.clear();
			g.reset();   
			
			std::cerr << err_msg << "\n";
		}
		
		
	}




}
